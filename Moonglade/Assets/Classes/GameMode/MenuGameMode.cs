﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuGameMode : GameMode {
    
    private void Start()
    {
        PlayerPrefs.SetInt("total_play", 0);
        GameAds.ShowBanner();
    }

    public void PlayClassicGameMode()
    {
        GameInstance.LoadLevel("level");
    }

    public void PlayTutorial()
    {
        GameInstance.LoadLevel("tutorial");
    }
    
}
