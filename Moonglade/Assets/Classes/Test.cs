﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Test : MonoBehaviour
{
    public List<int> grid;

    public int[] x;

    // Use this for initialization
    void Start()
    {
        Debug.Log(h(1, "fruits"));
        Debug.Log(h(2, "fruits"));
        Debug.Log(h(5, "fruits"));

        //Debug.Log(pow(2, 1000000000000000));
        Debug.Log("asdf "+ g("fruits"));
        //Debug.Log(h(pow(2, 1000000000000000), "fruits"));
        //Debug.Log(h(pow(2, 9831050005000007), "fruits"));


        /*
        Debug.Log("nomor 1: " + foo(x, 8, 18, 3, 6));
        Debug.Log("nomor 2: " + foo(x, 10, 20, 0, 9));
        Debug.Log("nomor 3: " + foo(x, 8, 18, 6, 3));
        Debug.Log("nomor 4: " + foo(x, 20, 10, 0, 9));
        Debug.Log("nomor 5: " + foo(x, 6, 7, 8, 8));*/

    }



    private string g(string str)
    {
        int i = 0;
        string new_str = "";
        while (i < str.Length - 1)
        {
            new_str = new_str + str[i + 1];
            i = i + 1;
        }

        return new_str;
    }

    private string f(string str)
    {
        if (str.Length == 0)
        {
            return "";
        }
        else if (str.Length == 1)
        {
            return str;
        }
        else
        {
            return f(g(str)) + str[0];
        }
    }

    private string h(int n, string str)
    {
        while (n != 1)
        {
            if (n % 2 == 0)
                n = n / 2;
            else
                n = 3 * n + 1;

            str = f(str);
        }

        return str;
    }

    private long pow(long x, long y)
    {
        if (y == 0)
            return 1;
        else
            return x * pow(x, y - 1);
    }

    private int test(int[] arr)
    {

        int i = 0;
        int s = 0;
        while (i < arr.Length)
        {
            s = s + arr[i];
            i = i + 1;
        }

        return s;
    }

    private int foo(int[] x, int a, int b, int i, int j)
    {
        int k = j;
        int ct = 0;

        while (k > i - 1)
        {
            if (x[k] <= b && !(x[k] <= a))
            {
                ct = ct + 1;
            }

            k = k - 1;
        }
        return ct;
    }

}
