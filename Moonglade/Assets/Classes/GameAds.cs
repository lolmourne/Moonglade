﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using GoogleMobileAds.Api;

public class GameAds : MonoBehaviour
{
    private InterstitialAd interstitial;
    private BannerView bannerView;

    private static GameAds instance;
    private bool isAdsShowing = false;
    private bool isAdsRequested = false;

    //private static string INTERSTITIAL_ID = "ca-app-pub-2554805079272720/2354421321";
    private static string INTERSTITIAL_ID = "ca-app-pub-9556734146322577/5522884106";
    //private static string BANNER_ID = "ca-app-pub-2554805079272720/4881635444";
    private static string BANNER_ID = "ca-app-pub-9556734146322577/3172052936";

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            MobileAds.Initialize("ca-app-pub-9556734146322577~7686013013");
            Debug.Log("Ads Initialized");
        }
        DontDestroyOnLoad(gameObject);
    }

    public static void HideBanner()
    {
        if (instance.bannerView != null)
        {
            instance.bannerView.Hide();
            instance.bannerView.Destroy();
        }
    }

    public static bool CanDoAction()
    {
        if (instance.isAdsRequested)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public static void ShowAds()
    {
        
        if (!Convert.ToBoolean(PlayerPrefs.GetInt("noads")))
        {
            instance.RequestInterstitial();
            instance.isAdsRequested = true;
        }
    }

    private void ShowInterstitial()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
            isAdsShowing = true;
            isAdsRequested = false;
        }
    }

    public static void ShowBanner()
    {

        if (!Convert.ToBoolean(PlayerPrefs.GetInt("noads")))
        {
            instance.RequestBanner();
        }
    }

    private void RequestBanner()
    {
        bannerView = new BannerView(BANNER_ID, AdSize.Banner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);
        Debug.Log("Request Banner Triggered");
    }

    private void RequestInterstitial()
    {
        interstitial = new InterstitialAd(INTERSTITIAL_ID);
        AdRequest request = new AdRequest.Builder().Build();

        interstitial.OnAdClosed += HandleOnClosing;
        interstitial.OnAdLoaded += HandleLoadedAds;
        interstitial.OnAdFailedToLoad += HandleOnClosing;
        interstitial.OnAdLeavingApplication += HandleOnClosing;
        interstitial.LoadAd(request);

        Debug.Log("Request Interstitial Triggered");
    }

    private void HandleOnClosing(object sender, EventArgs args)
    {
        isAdsShowing = false;
        isAdsRequested = false;
        interstitial.Destroy();
    }

    private void HandleLoadedAds(object sender, EventArgs args)
    {
        if (GameInstance.GetGameState() == GameState.PAUSE)
        {
            PlayerPrefs.SetInt("total_play", 0);
            ShowInterstitial();
        }
    }
}
