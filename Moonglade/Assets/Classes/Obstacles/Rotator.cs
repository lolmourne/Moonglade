﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Rotator : MonoBehaviour
{
    public List<RotatorElement> rotatorElements;

    private void Awake()
    {
        foreach(RotatorElement element in rotatorElements)
        {
            element.SetFirstRotation(element.rotatorObject.transform.eulerAngles);
        }
    }

    private void Update()
    {
        foreach(RotatorElement element in rotatorElements)
        {
            element.rotatorObject.transform.Rotate(0, 0, element.rotationSpeed * Time.deltaTime);
        }
    }

}

[System.Serializable]
public struct RotatorElement
{
    public float rotationSpeed;
    public GameObject rotatorObject;
    private Vector3 firstRotation;

    public void SetFirstRotation(Vector3 firstRotation)
    {
        this.firstRotation = firstRotation;
    }

    public Vector3 GetFirstRotation()
    {
        return firstRotation;
    }

}
